# MITW聯測用Auth Server

## 用途說明

此Auth Server介於Client與Resource Server(FHIR or Orthanc)之間，負責驗證Client的請求是否帶有合法的Authorization Token，驗證成功後才轉發請求至Resource Server取得資料。

並加入管理員使用的簡易控制台與api，可查看、新增、刪除授權。

## 環境需求

需事先準備好Node.js和npm的環境，或是docker。

### 下載

      $ git clone https://gitlab.com/FishYui/mitw-connectathon-gateway.git

### 執行前
- #### Env設定
  
  根據您的需求將 src/config/env/example.env改為:

      develop.env   開發、測試環境
      product.env   生產環境
  
  並將內容物依需求改寫:
    
      PORT                  指定服務端口
      FHIR_SERVER           指定FHIR API Server Base URL
      ORTHANC_SERVER        指定Orthanc API Server Base URL
      JWT_ISSUER            指定授權Token的發行者
      JWT_AUDIENCE          指定授權Token的讀者
      JWT_EXPIRED           指定Token的預設時效
      MANAGEMENT_USERNAME   管理員帳號
      MANAGEMENT_PASSWORD   管理員密碼

- #### JWT Token公私鑰設定
  
  此系統使用RS256作為JWT Token的簽發和驗證的演算法。<br>
  請將您自行產生的RSA金鑰對放入 src/config/serverKey 目錄當中，並將檔案名稱改為：

      pubkey.key    公鑰，用於驗證JWT Token
      privkey.key   私鑰，用於簽發JWT Token

  若為開發、測試環境可使用目錄中的公私鑰範例來做簽發和驗證<br>
  **<font color="#dd0000">注意-生產環境中請勿使用公私鑰範例</font>**

### 執行

- #### 一般執行

      $ cd mitw-connectathon-gateway/src
      $ npm install
      $ npm run dev     開發、測試環境
        or
      $ npm run start   生產環境

- #### 使用docker
    
    於根目錄
    
    docker建置預設是使用生產環境，若要使用測試環境請將Dockerfile最後一行改為

      CMD ["npm", "run", "dev"]
      
    執行

      $ docker build -t <Image 名稱> .
      $ docker run -d -p <外部 Port>:<Env Port> <Image 名稱>

- #### 測試
   
    建置完成後，打開 **http://&lt;Server IP>:<外部 Port>**<br>
    若看到 **Welcom to 2020 MITW Auth Server.** 字樣即為成功

### 使用方式

  - #### GateWay Api使用規則

        http://<Server IP>:<外部 Port>/gateway/<Server Type>/<Resource>/...options?

      FHIR Example：
        
        http://localhost:8080/gateway/fhir/Patient      (fhir/之後接FHIR搜尋規則)

      Orthanc Example：

        http://localhost:8080/gateway/orthanc/patients  (orthanc/之後接Orthanc搜尋規則)

      執行上述範例將會因為沒有附帶授權Token而回報錯誤

  - #### JWT Token使用規則

      將JWT Token放入**Authorization header**並使用**Bearer schema**

      Example：

        GET /gateway/fhir/Patient HTTP/1.1
        HOST: localhost:8080
        Authorization: Bearer <JWT Token>

  - #### 管理員用控制介面

    - **Login Page**

      控制台的登入介面，輸入Env設定的帳號及密碼即可登入，<br>
      登入成功後將跳轉至Permit List Page。

      - **URL：** `/login.html`

    - **Permit List Page**
     
      控制台的使用介面，可在此查看、新增、刪除授權，<br>
      需先登入。

      - **URL：** `/permitList.html`

### API Document

  - ### Home Api (Welecom Api)

    - **URL：** `/`
    - **Method：** `GET`
    - **Authorization header：** `No`
    - **Payload：** `No`

      - ### Response Success

        - **Status**： `200`
        - **response**：
          `Welcom to 2020 MITW Auth Server.`

  - ### 管理員登入

    - **URL：** `/login`
    - **Method：** `POST` 
    - **Authorization header：** `No`
    - **Payload：** 
      ```json
      {
        "username": "<Admin UserName>",
        "password": "<Admin PassWord>"
      }
      ```

      - ### Response Success

        成功登入

        - **Status：** `200`
        - **response：**
          ```json
          {
            "token": "<JWT Token(sub = admin)>"
          }
          ``` 
      
      - ### Response Fail

        帳號或密碼輸入錯誤

        - **Status：** `400`
        - **response：**
          ```json
          {
            "msg": "Username or Password Error."
          }
          ```

  - ### 取得所有通行證資訊

    - **URL：** `/permit`
    - **Method：** `GET` 
    - **Authorization header：** `Bearer <JWT Token(sub = admin)>`
    - **Payload：** `No`

      - ### Response Success

        成功取得

        - **Status：** `200`
        - **response：**
          ```json
          [
            {
              "sub": "Test",
              "exp": 1600522470,
              "jti": "49506a72-7343-4a1e-959c-.....",
              "token": "<JWT Token>"
            }
            .
            .
            .
          ]
          ``` 
      
      - ### Response Fail

        Authorization Error

        - **Status：** `401`

  - ### 取得一筆通行證資訊

    - **URL：** `/permit/:jti`
    - **Method：** `GET` 
    - **Authorization header：** `Bearer <JWT Token(sub = admin)>`
    - **Payload：** `No`

      - ### Response Success

        成功取得

        - **Status：** `200`
        - **response：**
          ```json
          {
            "sub": "Test",
            "exp": 1600522470,
            "jti": "49506a72-7343-4a1e-959c-.....",
            "token": "<JWT Token>"
          }
          ``` 
      
      - ### Response Fail

        Authorization Error

        - **Status：** `401`

  - ### 新增一筆通行證資訊

    - **URL：** `/permit`
    - **Method：** `POST` 
    - **Authorization header：** `Bearer <JWT Token(sbu = admin)>`
    - **Payload：** 
      ```json
      {
        "sub": "<組織/單位名稱> ex: MITW",
        "exp": "<過期日期的時間戳> ex: 1604160000"
      }
      ```

      - ### Response Success

        新增成功

        - **Status：** `200`
        - **response：**
          ```json
          {
            "sub": "MITW",
            "exp": 1604160000,
            "jti": "2dc69737-f0ef-4c0f-b6e5-098b2f5ecc5c",
            "token": "<JWT Token>"
          }
          ``` 
      
      - ### Response Fail

        sub或exp遺漏

        - **Status：** `400`
        - **response：**
          ```json
          {
            "msg": "Subject or Expired Undefine."
          }
          ```

        sub輸入admin，不可在此新增admin的token

        - **Status：** `400`
        - **response：**
          ```json
          {
            "msg": "Can't append Subject name: Admin."
          }
          ```

        Authorization Error

        - **Status：** `401`

  - ### 刪除一或多筆通行證資訊

    - **URL：** `/permit/:jti  (/permit/jti1,jti2,jti3...)`
    - **Method：** `DELETE` 
    - **Authorization header：** `Bearer <JWT Token(sbu = admin)>`
    - **Payload：** `No`

      - ### Response Success

        成功刪除

        - **Status：** `200`
        - **response：**
          ```json
          {}
          ``` 
      
      - ### Response Fail

        Authorization Error

        - **Status：** `401`

  - ### 取得FHIR資源

    - **URL：** `/gateway/fhir/*(fhir_rule)`
    - **Method：** `GET` 
    - **Authorization header：** `Bearer <JWT Token>`
    - **Payload：** `No`

      - ### Response Success

        成功取得

        - **Status：** `200`
        - **response：**
          ```json
          {
            "resourceType": "Bundle",
            "id": "8b91325f-30be-415e-a638-6ac67f9266bd",
            "meta": {
              "lastUpdated": "2020-09-07T20:36:39.657+08:00"
            },
            "type": "searchset",
            .
            .
            .
          }
          ```
      
      - ### Response Fail

        FHIR搜尋規則錯誤

        - **Status：** `404`
        - **response：**
          ```json
          {
            "resourceType": "OperationOutcome",
            "text": {
              "status": "generated",
              "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h1>Operation Outcome</h1><table border=\"0\"><tr><td style=\"font-weight: bold;\">ERROR</td><td>[]</td><td><pre>Unknown resource type 'abc' - Server knows how to handle: [Appointment, Account, Invoice, CatalogEntry, EventDefinition, DocumentManifest, MessageDefi..."
            .
            .
            .
          }
          ```

        Authorization Error

        - **Status：** `401`

  - ### 取得Orthanc資源

    - **URL：** `/gateway/orthanc/*(orthanc_rule)`
    - **Method：** `GET` 
    - **Authorization header：** `Bearer <JWT Token>`
    - **Payload：** `No`

      - ### Response Success

        成功取得

        - **Status：** `200`
        - **response：**
          ```json
          [
            "cbb79657-e546f3de-d437bdda-......",
            "c2d4c545-2f59cff5-973dd9d0-......",
            "9b5cf30f-ae0bc464-c2053b82-......",
            "cc986458-4d993376-1b3a1e0b-......",
            "e00865e4-9f8bbe26-7d352d61-......",
            .
            .
            .
          ]
          ```
      
      - ### Response Fail

        Orthanc搜尋規則錯誤

        - **Status：** `404`
        - **response：**
          ```json
          {
            "HttpError": "Not Found",
            "HttpStatus": 404,
            "Message": "Unknown resource",
            "Method": "GET",
            "OrthancError": "Unknown resource",
            "OrthancStatus": 17,
            "Uri": "/abc"
          }
          ```

        Authorization Error

        - **Status：** `401`

  - ### Authorization 錯誤

    - #### 沒有Authorization header：

      請求api時並沒有附加Authorization header。

      - **Authorization header：** `undefine`
      - **Response Code：** `401`
  
        ```json
        {
          "msg": "Not Found Authorization Header."
        }
        ```

    - #### 有Authorization header但是格式錯誤：
     
      請求api時有附加Authorization header，<br>
      但是格式錯誤如`Bearer " "` or `" " <JWT Token>` or `Bearer`拼錯。

      - **Authorization header：** `<no Bearer> token...`
      - **Response Code：** `401`
  
        ```json
        {
          "msg": "Authorization Header Error."
        }
        ```

    - #### Authorization Token被竄改：

      使用竄改後的Token去呼叫api。

      - **Authorization header：**
        
        original

            Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL29hdXRoLmRpY29tLm9yZy50dyIsInN1YiI6IuS4iei7jee4vemGq-mZoiIsImF1ZCI6IjE1MC4xMTcuMTIxLjY3IiwiaWF0IjoxNTk5MjkzNzEzLCJleHAiOjE2MDQxNjAwMDAsImp0aSI6IjAwMSJ9.YKKKDR4ndf2BLfinMJ-u1M7LLYswUYP3fSTFIrsbTpVWjQnCS2hSgW5qK78pSBzFvtrpklYejLHpwCZD5f6d9T_1mhKu-iSilbY2Nk5mGtqVpym7Uic7-zpkdPdMLdDBw3jtu9IgHqBhQFjUv6701H7KaS0CLJAHmAERcZkPBWQWPJvs8s1hUFq93MHVgKJs6BcO_kNtFU3UV7KDUgujHUdsFp622ZFmLRYeWb2lYGsX14hjxR2phEEmQ8g-3ES2Yhfl3XcWKmeGEU5qUprtmKAYMEu9g-e_XSaT4p-VRk_E37U66ZdF5wfjE6SJtISySvYseBVNt9K8_4ZCFnpRbQ 

        new

            Bearer AAAeyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL29hdXRoLmRpY29tLm9yZy50dyIsInN1YiI6IuS4iei7jee4vemGq-mZoiIsImF1ZCI6IjE1MC4xMTcuMTIxLjY3IiwiaWF0IjoxNTk5MjkzNzEzLCJleHAiOjE2MDQxNjAwMDAsImp0aSI6IjAwMSJ9.YKKKDR4ndf2BLfinMJ-u1M7LLYswUYP3fSTFIrsbTpVWjQnCS2hSgW5qK78pSBzFvtrpklYejLHpwCZD5f6d9T_1mhKu-iSilbY2Nk5mGtqVpym7Uic7-zpkdPdMLdDBw3jtu9IgHqBhQFjUv6701H7KaS0CLJAHmAERcZkPBWQWPJvs8s1hUFq93MHVgKJs6BcO_kNtFU3UV7KDUgujHUdsFp622ZFmLRYeWb2lYGsX14hjxR2phEEmQ8g-3ES2Yhfl3XcWKmeGEU5qUprtmKAYMEu9g-e_XSaT4p-VRk_E37U66ZdF5wfjE6SJtISySvYseBVNt9K8_4ZCFnpRbQ   
      - **Response Code：** `401`
  
        ```json
        {
          "msg": "invalid token"
        }
        ```

    - #### Authorization Token時效已過：

      使用已經過期的Token去呼叫api。

      - **Authorization header：**
      
            Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL29hdXRoLmRpY29tLm9yZy50dyIsInN1YiI6Iua4rOippueUqCIsImF1ZCI6IjE1MC4xMTcuMTIxLjY3IiwiaWF0IjoxNTk5MjkzNzEzLCJleHAiOjE1OTkyOTM3MTQsImp0aSI6IjAwMSJ9.yZ5Y2_6G2KXg6pbl54tbt-WM8xc14S6NWwvn7P65v_hDtcBlxjLV2NIoHPAMKxl3cvSrAgy98UcmoE5qOvijVJR9LeCKiqD5tmzMAsC_4WDEf6_d2wHK-Ij_s6F_4maJ8AiEtLhxkGVV1EWBGa7TfeujRiU0eOzNPJLnOhpiqUvEZJKDZUOh2MflQoa4SzZBfwEjkIcjh70dd98pXg9YcQU257d_Q8VApiaJs3ylwqjfNZn228cN4GsQgwYTsKWTuJnehABu6nj7BfcPiLQEm1XdgMukxGDo6RObooCR64pcz5PxVhX1_TktqylU-IyF-0x7ymeHCGY-_gd1_p5lrA

      - **Response Code：** `401`
  
        ```json
        {
          "msg": "jwt expired"
        }
        ```

    - #### Authorization Token不存在：

      因為某些原因Token被管理員從白名單刪除後，<br>
      在用此Token呼叫api，將會觸發此錯誤。
      
      - **Authorization header：** `<JWT Token>`
      - **Response Code：** `401`
  
        ```json
        {
          "msg": "Authorization Is Cancelled."
        }
        ```    

    - #### 權限不足：

      使用非admin的Token去呼叫permit的api，將會觸發此錯誤。

      - **Authorization header：** `<JWT Token(sub != admin)>`
      - **Response Code：** `401`
  
        ```json
        {
          "msg": "Not Authorized to Access."
        }
        ```    
