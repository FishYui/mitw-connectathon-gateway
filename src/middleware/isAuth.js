/*
  檢驗Authorization token
*/

const jwtService = require("../services/jwtService");
const permitService = require("../services/permitService");
const permitServiceInstance = new permitService();

module.exports = async (req, res, next) => {

  const { authorization } = req.headers;
  if (!authorization) {
    const newError = new Error(`Not Found Authorization Header.`);
    newError.status = 401;
    return next(newError);
  }

  const token = authorization.split(" ");
  if (token.length != 2 || token[0] != "Bearer") {
    const newError = new Error(`Authorization Header Error.`);
    newError.status = 401;
    return next(newError);
  }

  try {

    const payload = await jwtService.verify(token[1]);
    const permit = await permitServiceInstance.findOne(payload.jti);
    if(!permit) {
      const newError = new Error(`Authorization Is Cancelled.`);
      newError.status = 401;
      return next(newError);
    }

    req.jwtPayload = payload;
    next();

  } catch (error) {
    next(error);
  }

}