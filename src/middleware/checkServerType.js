/*
  檢查Server Type是否是Auth Server已知的
  並將Request Auth Api Url組合成Request Server Url(fhir or orthanc)
*/

const config = require("../config");

module.exports = (req, res, next) => {

  const serverType = req.params.server_type;
  const reqUrl = req.url;
  const server = config.server.find(server => server.server_type == serverType);

  if (server) {
    req.reqUrl = server.url + reqUrl.replace(`/${serverType}`, "");
    next();
  }
  else {
    const newError = new Error(`Not Support ${serverType} Server.`);
    newError.status = 404;
    next(newError);
  }

}