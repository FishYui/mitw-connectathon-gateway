/*
  錯誤回傳處理
*/

module.exports = (err, req, res, next) => {

  const jwtErrorName = ["TokenExpiredError", "JsonWebTokenError", "NotBeforeError"];
  if (jwtErrorName.find(name => name == err.name)) err.status = 401;

  if (!err.status) err.status = 500;

  const { status, message } = err;
  return res.status(status).json({ msg: message });
}