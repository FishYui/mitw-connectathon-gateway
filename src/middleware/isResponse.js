/*
  統一回傳處理
*/

module.exports = (req, res) => {

  const { status, data, headers } = req.response;

  if(headers && headers['content-type']) {
    res.set({'content-type': headers['content-type']});
  }

  res.status(status).send(data);
  
  return;

}