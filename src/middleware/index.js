const checkServerType = require("./checkServerType");
const isAuth = require("./isAuth");
const noPage = require("./noPage");
const isResponse = require("./isResponse");
const errorHandler = require("./errorHandler");

module.exports = {
  checkServerType,
  isAuth,
  noPage,
  isResponse,
  errorHandler
}