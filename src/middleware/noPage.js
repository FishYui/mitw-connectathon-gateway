/*
  檢查request api是否存在
*/

module.exports = (req, res, next) => {

  if (req.route) next();
  else {
    const newError = new Error(`Not Found.`);
    newError.status = 404;
    next(newError);
  }
}