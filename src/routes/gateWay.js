const express = require('express');
const router = express.Router();
const httpRequest = require("../services/httpRequest");
const middleware = require("../middleware");

router.all('/:server_type*', middleware.checkServerType, middleware.isAuth, async (req, res, next) => {
  
  const { method, reqUrl, headers, body } = req;
  
    try {

    const response = await httpRequest(method, reqUrl, headers, body);
    req.response = response;
    next();

  } catch (err) {
    console.debug(err);
    next(err);
  }

});

module.exports = router;