const express = require("express");
const router = express.Router();
const permitService = require("../services/permitService");
const permitServiceInstance = new permitService();
const middleware = require("../middleware");

router.all("/*", middleware.isAuth, async (req, res, next) => {

  try {

    if (req.jwtPayload.sub != "Admin") {
      const newError = new Error("Not Authorized to Access.");
      newError.status = 401;
      return next(newError);
    }

    next();

  } catch (error) {
    console.debug(error);
    next(error);
  }

});

router.get("/", async (req, res, next) => {

  try {

    const result = await permitServiceInstance.findAll();
    const response = { status: 200, data: result }
    req.response = response;
    next();

  } catch (error) {
    console.debug(error);
    next(error);
  }

});

router.get("/:jti", async (req, res, next) => {

  try {

    const result = await permitServiceInstance.findOne(req.params.jti);
    const response = { status: 200, data: result }
    req.response = response;
    next();

  } catch (error) {
    console.debug(error);
    next(error);
  }

});

router.post("/", async (req, res, next) => {

  try {

    const { sub, exp } = req.body;

    if (!sub || sub == " " || !exp || exp == " ") {
      const newError = new Error("Subject or Expired Undefine.");
      newError.status = 400;
      return next(newError);
    }

    if (sub == "Admin") {
      const newError = new Error("Can't append Subject name: Admin.");
      newError.status = 400;
      return next(newError);
    }

    const result = await permitServiceInstance.pushOne({ sub, exp });
    const response = { status: 200, data: result }
    req.response = response;
    next();

  } catch (error) {
    console.debug(error);
    next(error);
  }

});

router.delete("/:jti", async (req, res, next) => {

  try {

    const jtiList = req.params.jti.split(",")
    await permitServiceInstance.remove(jtiList);

    const response = { status: 200, data: {} }
    req.response = response;
    next();

  } catch (error) {
    console.debug(error);
    next(error);
  }

});

module.exports = router;