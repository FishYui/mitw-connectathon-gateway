const home = require("./home");
const gateway = require("./gateWay");
const permit = require("./permit");
const middleware = require("../middleware");

const routes = (app) => {

  app.use("/", home)
  app.use("/gateway", gateway);
  app.use("/permit", permit);
  
  app.use("*", middleware.noPage);

  app.use(middleware.isResponse);
  app.use(middleware.errorHandler);
}

module.exports = routes;