/*
    Home
*/

const express = require('express');
const router = express.Router();
const permitService = require("../services/permitService");
const permitServiceInstance = new permitService();
const config = require("../config");

router.post("/login", async (req, res, next) => {

    try {

        const { username, password } = req.body;
        if (username != config.management.username || password != config.management.password) {
            const newError = new Error("Username or Password Error.");
            newError.status = 400;
            return next(newError);
        }

        const data = { "sub": "Admin", "exp": (Date.now() / 1000) + config.jwtToken.expired * 60 }
        const result = await permitServiceInstance.pushOne(data);

        const response = { status: 200, data: { token: result.token } }
        req.response = response;

        next();

    } catch (error) {
        console.debug(error);
        next(error);
    }

});

module.exports = router;