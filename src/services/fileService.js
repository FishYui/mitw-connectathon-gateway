/*
    file sevice
*/

const fs = require("fs");
const path = require("path");

class fileService {

    constructor(path) {

        this.path = path;
        this.isFileExist();
    }

    async read() {

        try {

            const data = await fs.readFileSync(this.path).toString();

            switch (path.extname(this.path)) {
                case ".json":
                    return data ? JSON.parse(data) : [];
                case ".txt":
                    return data ? data : "";
                default:
                    return data;
            }

        } catch (error) {
            throw error;
        }

    }

    async write(data) {

        try {

            await fs.writeFileSync(this.path, data);

        } catch (error) {
            throw error;
        }

    }

    async isFileExist() {

        try {

            if (fs.existsSync(this.path)) return;

            if (!path.extname(this.path)) {
                throw new Error(`${this.path} not a File.`);
            }

            const dirPath = path.dirname(this.path);
            this.mkDir(dirPath);

            await fs.writeFileSync(this.path, "");

        } catch (error) {
            throw error;
        }

    }

    async mkDir(dirPath) {

        try {

            if (fs.existsSync(dirPath)) return;

            this.mkDir(path.resolve(dirPath, ".."));
            await fs.mkdirSync(dirPath);

        } catch (error) {
            throw error;
        }

    }
}

module.exports = fileService;