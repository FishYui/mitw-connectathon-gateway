/*
  permit handle
*/

const { v4: uuidv4 } = require("uuid");
const config = require("../config");
const jwtService = require("./jwtService");
const fileService = require("./fileService");

class permitManagement {

  constructor() {

    this.fileServiceInstance = new fileService("./config/permit/data.json");
  }

  async findAll() {

    try {

      const permitList = await this.fileServiceInstance.read();

      return permitList;

    } catch (error) {
      throw error;
    }

  }

  async findOne(jti) {

    try {

      const permitList = await this.fileServiceInstance.read();
      const permit = permitList.find(permit => permit.jti == jti);

      return permit;

    } catch (error) {
      throw error;
    }

  }

  async pushOne(obj) {

    try {

      const payload = {
        iss: config.jwtToken.issuer,
        sub: obj.sub,
        aud: config.jwtToken.audience,
        iat: Math.floor(Date.now() / 1000),
        exp: parseInt(obj.exp),
        jti: uuidv4(),
      }
      const newToken = await jwtService.sign(payload);


      const newPermit = {
        sub: payload.sub,
        exp: payload.exp,
        jti: payload.jti,
        token: newToken
      }
      const permitList = await this.fileServiceInstance.read();
      permitList.push(newPermit);

      await this.fileServiceInstance.write(JSON.stringify(permitList));

      return newPermit;

    } catch (error) {
      throw error;
    }

  }

  async remove(jti) {

    try {

      const jtiList = !Array.isArray(jti) ? [jti] : jti;
      const permitList = await this.fileServiceInstance.read();
      const newPermitList = permitList.filter(permit => {
        if (!jtiList.includes(permit.jti)) return permit;
      });

      if (permitList.length != newPermitList.length)
        await this.fileServiceInstance.write(JSON.stringify(newPermitList));

    } catch (error) {
      throw error;
    }

  }

  async removeExpired() {

    try {

      const now = Date.now() / 1000;
      const permitList = await this.fileServiceInstance.read();
      const newPermitList = permitList.filter(permit => {
        if (permit.exp > now) return permit;
      });

      if (permitList.length != newPermitList.length)
        await this.fileServiceInstance.write(JSON.stringify(newPermitList));

    } catch (error) {
      throw error;
    }

  }

}

module.exports = permitManagement;