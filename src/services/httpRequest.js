/*
  axios http request
*/

const { default: Axios } = require("axios");

module.exports = async (method = "get", url, headers, params = {}) => {

  delete headers.host;
  try {

    const response = await Axios({
      method: method,
      url: url,
      headers,
      data: params
    });

    return response;

  } catch (err) {

    if (err.response) {
      return err.response;
    }
    else {
      throw err;
    }
  }

}