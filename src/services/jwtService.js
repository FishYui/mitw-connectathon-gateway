/*
  jwt service
*/

const jwt = require("jsonwebtoken");
const config = require("../config");

async function sign(pay) {

  try {

    const payload = pay || {};
    const token = await jwt.sign(payload, config.jwtToken.privateKey, { algorithm: "RS256" });

    return token;

  } catch (error) {
    throw error;
  }

}

async function verify(token) {

  try {

    const payload = await jwt.verify(token, config.jwtToken.publicKey, { algorithms: "RS256" });

    return payload;

  } catch (error) {
    throw error;
  }

}


module.exports = { sign, verify };