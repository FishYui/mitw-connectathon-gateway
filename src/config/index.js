module.exports = {
  port: process.env.PORT || 8080,

  jwtToken: {
    publicKey: "",
    privateKey: "",
    issuer: process.env.JWT_ISSUER,
    audience: process.env.JWT_AUDIENCE,
    expired: process.env.JWT_EXPIRED
  },

  server: [
    { server_type: "fhir", name: "fhir server", url: process.env.FHIR_SERVER },
    { server_type: "orthanc", name: "orthanc server", url: process.env.ORTHANC_SERVER }
  ],

  management: {
    username: process.env.MANAGEMENT_USERNAME,
    password: process.env.MANAGEMENT_PASSWORD
  }
}