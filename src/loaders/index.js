/*
  載入各式middleware
*/

const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const fs = require("fs");
const config = require("../config");
const routes = require("../routes");

module.exports = async () => {

  try {
    const app = express();

    app.use(cors());
    app.use(bodyParser.json({type: ['application/json', 'application/fhir+json', 'application/dicom+json'], limit : '60mb'}));
    app.use(bodyParser.raw({type: 'application/octet-stream', limit : '60mb'}));
    app.use(express.static("public"));

    const pubkey = await fs.readFileSync("./config/serverKey/pubkey.key");
    config.jwtToken.publicKey = pubkey;

    const privkey = await fs.readFileSync("./config/serverKey/privkey.key");
    config.jwtToken.privateKey = privkey;

    routes(app);

    (function () {

      const permitService = require("../services/permitService");
      const permitServiceInstance = new permitService();

      setInterval(() => { permitServiceInstance.removeExpired(); }, config.jwtToken.expired * 60 * 1000);
    })();

    app.listen(config.port, () => {
      console.log(`System running on PORT:${config.port}`);
    });
  } catch (e) {
    console.debug(e);
    return;
  }

};
