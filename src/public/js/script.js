function getCookie(name) {

    const cookies = document.cookie.split(";");

    for(let cookie of cookies) {
        const temp = cookie.split("=");
        if(temp[0].replace(/(^\s*)|(\s*$)/g,"") == name) return temp[1];
    }

    return;
}
