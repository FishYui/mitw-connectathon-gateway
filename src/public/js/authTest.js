function search() {

  const token = document.querySelector('#inputToken').value;
  const url = document.querySelector('#inputUrl').value;
  const payload = document.querySelector('#payload');
  const response = document.querySelector('#response');
  const responseStatus = document.querySelector('#response-status');

  payload.innerHTML = "";
  response.innerHTML = "";

  (async () => {

    try {

      const result = await fetch(`${url}`, {
        headers: {
          "Authorization": `Bearer ${token}`
        }
      });

      const contentType = result.headers.get('content-type').split(';')[0];

      switch (contentType) {
        case 'application/json':
        case 'application/fhir+json':
        case 'application/dicom+json':
          const json = await result.json();
          response.innerHTML = JSON.stringify(json, null, 2);
          break;
        default:
          const blob = await result.blob();
          const file = window.URL.createObjectURL(blob);
          window.location.assign(file);
          break;
      }

      const payloadStr = decodeURIComponent(escape(window.atob(token.split('.')[1].replace(/-/g, "+").replace(/_/g, "/"))));
      payload.innerHTML = JSON.stringify(JSON.parse(payloadStr), null, 2);
      responseStatus.innerHTML = `, Status: ${result.status}`;

      return;

    } catch (error) {
      alert("Server Error.");
      response.innerHTML = error.message;
      return;
    }
  })();

}