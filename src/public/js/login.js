function login() {

    const formMessage = document.querySelector(".form-message");
    formMessage.innerHTML = "";
    const username = document.querySelector("#inputUsername").value;
    const password = document.querySelector("#inputPassword").value;

    (async () => {

        try {

            const response = await fetch(`${authServer}/login`, {
                method: "POST",
                body: JSON.stringify({ username, password }),
                headers: {
                    "Content-Type": "application/json"
                }
            });

            const json = await response.json();

            if (!response.ok) {
                return formMessage.innerHTML += `<div class="alert alert-dismissable alert-danger">${json.msg}</div>`;
            }

            document.cookie = `MITWADMIN=${json.token}`;
            window.location = "./permitList.html";

        } catch (error) {
            alert("Server Error.");
            console.error(error);
            return;
        }
    })();
}
