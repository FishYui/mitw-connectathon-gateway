(function main() {

    const token = getCookie("MITWADMIN");

    if (!token) {
        alert("請先登入取得授權.");
        return window.location = "./login.html";
    }

    window.onload = function () {
        getPermitList();
    }

})();

function getPermitList() {

    const listBody = document.querySelector(".permit-list-body");
    listBody.innerHTML = "";

    (async () => {

        try {

            const response = await fetch(`${authServer}/permit`, {
                headers: {
                    "Authorization": `Bearer ${getCookie("MITWADMIN")}`
                }
            });

            const json = await response.json();

            if (response.status == 401) {
                alert(`Error Message: ${json.msg}\n請重新登入`);
                return window.location = "./login.html";
            }

            if (!response.ok) {
                alert(`Error Message: ${json.msg}`);
                return;
            }

            json.forEach((item, index) => {

                const num = index + 1;
                const id = item.jti;
                const sub = item.sub;
                const exp = ((sec => {
                    const date = new Date(sec * 1000);
                    return `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
                }))(item.exp);
                const token = item.token;

                listBody.innerHTML += `
                <tr>
                    <td class="num">${num}</td>
                    <td class="id">${id}</td>
                    <td class="sub">${sub}</td>
                    <td class="exp ${new Date().getTime() / 1000 > item.exp ? 'over' : ''}">${exp}</td>
                    <td class="token">${token}</td>
                    <td class="tool">
                        <button class="btn btn-danger" onclick="deletePermit(this, '${id}')">刪除</button>
                    </td>
                </tr>`;
            });

        } catch (error) {
            alert("Server Error.");
            console.error(error);
            return;
        }
    })();
}

function appendPermit() {

    const formMessage = document.querySelector(".form-message");
    formMessage.innerHTML = "";
    const subject = document.querySelector("#inputSub").value;
    const expiration = document.querySelector("#inputExp").value;

    (async () => {

        try {

            const response = await fetch(`${authServer}/permit`, {
                method: "POST",
                body: JSON.stringify({
                    sub: subject,
                    exp: new Date(expiration).getTime() / 1000
                }),
                headers: {
                    "Authorization": `Bearer ${getCookie("MITWADMIN")}`,
                    "Content-Type": "application/json"
                }
            });

            const json = await response.json();

            if (response.status == 401) {
                alert(`Error Message: ${json.msg}\n請重新登入`);
                return window.location = "./login.html";
            }

            if (!response.ok) {
                return formMessage.innerHTML += `<div class="alert alert-dismissable alert-danger">${json.msg}</div>`;
            }

            const num = document.querySelector(".permit-list-body").getElementsByTagName("tr").length + 1;
            const id = json.jti;
            const sub = json.sub;
            const exp = ((sec => {
                const date = new Date(sec * 1000);
                return `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
            }))(json.exp);
            const token = json.token;

            document.querySelector(".permit-list-body").innerHTML += `
            <tr>
                <td class="num">${num}</td>
                <td class="id">${id}</td>
                <td class="sub">${sub}</td>
                <td class="exp ${new Date().getTime() / 1000 > json.exp ? 'over' : ''}">${exp}</td>
                <td class="token">${token}</td>
                <td class="tool">
                    <button class="btn btn-danger" onclick="deletePermit(this, '${id}')">刪除</button>
                </td>
            </tr>`;

        } catch (error) {
            alert("Server Error.");
            console.error(error);
            return;
        }
    })();
}

function deletePermit(e, id) {

    (async () => {

        try {

            const response = await fetch(`${authServer}/permit/${id}`, {
                method: "DELETE",
                headers: {
                    "Authorization": `Bearer ${getCookie("MITWADMIN")}`,
                    "Content-Type": "application/json"
                }
            });

            const json = await response.json();

            if (response.status == 401) {
                alert(`Error Message: ${json.msg}\n請重新登入`);
                return window.location = "./login.html";
            }

            if (!response.ok) {
                alert(`Error Message: ${json.msg}`);
                return;
            }

            document.querySelector(".permit-list-body").removeChild(e.parentNode.parentNode);

        } catch (error) {
            alert("Server Error.");
            console.error(error);
            return;
        }
    })();
}

function hiddenAdmin(e) {

    const adminPermit = document.querySelectorAll(".sub");
    const hidden = e.classList.contains('hidden-true');
    
    e.classList.replace(`hidden-${hidden}`, `hidden-${!hidden}`);

    for (let permit of adminPermit) {
        if (permit.innerHTML == "Admin") {
            permit.parentNode.hidden = !hidden;
        }
    }
    
}