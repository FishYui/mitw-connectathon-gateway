async function startServer() {

  process.env.NODE_ENV = process.env.NODE_ENV || "develop";
  if (process.env.NODE_ENV === "develop")
    require("dotenv").config({ path: "./config/env/develop.env" });
  else if (process.env.NODE_ENV === "product")
    require("dotenv").config({ path: "./config/env/product.env" });

  await require("./loaders/index")();

}

startServer();